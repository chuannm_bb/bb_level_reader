import * as fs from 'fs';
import * as jsonfile from 'jsonfile';
import * as path from 'path';
import LevelReader from './LevelReader';
// const levelData = LevelReader.instance.read(level1);

// const b = LevelReader.instance.encode(levelData);
// console.log('encode: %s', b);
// const items = LevelReader.instance.decode(b);
// console.log('items: %s', JSON.stringify(items));

var files = fs.readdirSync('assets');
var levels = files.map(m => {
    var p = path.join('assets', m);
    var json = jsonfile.readFileSync(p);
    return LevelReader.instance.read(json);
}).sort((a, b) => {
    return a.levelName.localeCompare(b.levelName);
}).map(m => LevelReader.instance.encode(m));

jsonfile.writeFileSync('all-level.json', levels);
