let instance: LevelReader;
function btoa(s: string): string {
    return Buffer.from(s).toString('base64');
}
function atob(s: string): string {
    return Buffer.from(s, 'base64').toString();
}

export default class LevelReader {
    static get instance() {
        return instance || (instance = new LevelReader());
    }


    read(json: Object | string): LevelData {
        if (typeof json === 'string') {
            json = JSON.parse(json);
        }
        const data = Object.assign(new LevelData(), json);
        data.groups = data.groups || [];
        data.groups = data.groups.sort((a, b) => {
            const abase = a.allowValues.every(i => i > 0 && i < 7), bbase = b.allowValues.every(i => i > 0 && i < 7);
            if (!abase || !bbase) {
                if (abase) return -1;
                if (bbase) return 1;
            }
            return a.rate === b.rate ? a.allowValues.length - b.allowValues.length : a.rate - b.rate;
        });

        return data;
    }
    encode(lvData: LevelData) {
        const cubes = lvData.cubes;
        let buff = [];
        for (let cube of cubes) {
            var ext = cube.extFrame & 0x0f << 4 | cube.extBubble & 0x0f;
            buff.push(cube.base, ext);
        }

        const data = [
            lvData.levelName,
            lvData.width,
            lvData.height,
            lvData.move,
            lvData.goals.map(n => n.name + "," + n.count ).join(";"),
            lvData.groups.map(n => [n.name, n.rate, n.allowValues.join(',')].join(':')).join(';'),
            lvData.scores?.map(n => n).join(","),
            btoa(String.fromCharCode(...buff))
        ]
            
        return data.join("|");
        
    }
    decode(b: string): LevelData {
        var r = new LevelData();
        let data: CubeData[] = []
        const a = b.split('|');
        r.fileName = a[0]
        r.width = parseInt(a[1]);
        r.height = parseInt(a[2]);
        r.move = parseInt(a[3]);
        r.goals = a[4].split(';').map(s => {
            const a = s.split(',');
            return  {name: a[0], count: parseInt(a[1])}
        });
        r.groups = a[5].split(';').map(s => {
            const a = s.split(':');
            return {
                name: a[0],
                rate: parseFloat(a[1]),
                allowValues: a[2].split(',').map(i => parseInt(i))
            };
        });
        let s = atob(a[6]);
        for (let i = 0, n = s.length; i < n; i += 2) {
            let cube = new CubeData(), ext = s.charCodeAt(i + 1);
            cube.base = s.charCodeAt(i);
            cube.extFrame = ext >> 4;
            cube.extBubble = ext & 0x0f;
            data.push(cube);
        }
        r.cubes = data;
        return r;
    }
}

export class GoalData {
    name = '';
    count = 0;
}

export class LevelData {
    fileName = '';
    levelName = '';
    width = 0;
    height = 0;
    move = 0;
    goals: GoalData[] = [];
    cubes: CubeData[] = [];
    groups: GroupData[] = [];
    scores?: number[];
}
export class GroupData {
    name = '';
    rate = 0;
    allowValues: number[] = [];
}

export class CubeData {
    base = 0;
    extFrame = 0;
    extBubble = 0;
}
